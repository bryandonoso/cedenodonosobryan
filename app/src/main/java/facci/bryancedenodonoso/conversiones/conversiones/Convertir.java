package facci.bryancedenodonoso.conversiones.conversiones;

class Convertir {
    public static double CelsiusAFare (double f){
        return (f - 32)*5/9;
    }
    public static double FareACelsius (double c){
        return 32+c*9/5;
    }
}
