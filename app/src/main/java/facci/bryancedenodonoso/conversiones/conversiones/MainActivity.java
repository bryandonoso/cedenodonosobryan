package facci.bryancedenodonoso.conversiones.conversiones;

import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText temperatura;
    TextView resultado;
    Button conversion;
    RadioButton celciAfare, fareAcelci;

    public  static final String TAG = MainActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG,"Cedeño Donos Bryan Alex");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        temperatura = (EditText)findViewById(R.id.valor);
        resultado = (TextView)findViewById(R.id.resultado);
        conversion = (Button)findViewById(R.id.convertir);
        celciAfare = (RadioButton)findViewById(R.id.celciAfare);
        fareAcelci = (RadioButton)findViewById(R.id.fareAcelci);

        conversion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double temp = new Double(temperatura.getText().toString());
                if (fareAcelci.isChecked())
                    temp = Convertir.CelsiusAFare(temp);
                else temp = Convertir.FareACelsius(temp);
                resultado.setText(new Double(temp).toString());
            }
        });


    }
}
